#!/bin/sh

BASEDIR=$(readlink "$(dirname "$0")")
STACK="$1"
DATACENTER="$2"
CERTDIR=${CERTDIR:-"$HOME/.blackfish/$STACK"}

if [ -z "$STACK" ] || [ -z "$DATACENTER" ]; then
    echo "usage: $BASEDIR/blackfish.sh STACK DATACENTER" >&2
    echo "It will generate the CA cert and a client cert in the $HOME/.blackfish/STACK directory." >&2
    exit 1
fi

mkdir -p "$CERTDIR"

## CN,OU,Hostnames must be named after flocker's convention
## Ask flockerz : https://github.com/ClusterHQ/flocker/blob/master/flocker/ca/_ca.py
## Warning! Flocker impl doesn't support wildcard DNS.

OU=$(uuidgen)

cat > "$CERTDIR"/run.sh <<EOF
#!/bin/bash
set -e
pushd $CERTDIR >/dev/null 2>&1
if [ ! -f "$CERTDIR/ca.pem" ]; then
   echo "generating CACERT" >&2
   echo '{"names":[{"OU":"$OU"}],"CN":"$STACK","key":{"algo":"rsa","size":2048}}' | cfssl gencert -initca - | cfssljson -bare ca -
else
   echo "ca.pem exists. keeping previous cert. delete first if you want to regenerate it" >&2
fi

if [ ! -f "$CERTDIR/cert.pem" ]; then
  echo '{"signing":{"default":{"expiry":"8760h","usages":["client auth"]}}}' > req.json
  echo "generating CLIENT Cert" >&2
  echo '{"CN":"user-$STACK","names":[{"OU":"$OU"}],"key":{"algo":"rsa","size":2048}}' | cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=req.json - | cfssljson -bare "client"
  # move files for docker DOCKER_CERT_PATH compatibiliy
  mv client.pem cert.pem
  mv client-key.pem key.pem
else
   echo "user cert.pem exists. keeping previous cert. delete first if you want to regenerate it" >&2
fi

if [ ! -f "$CERTDIR/wildcard-user-$DATACENTER-services.pem" ]; then
  echo '{"signing":{"default":{"expiry":"8760h","usages":["signing","server auth"]}}}' > req.json
  echo '{"names":[{"OU":"$OU"}],"CN":"user-services","hosts":["*.service.$STACK","*.service.$DATACENTER.$STACK"],"key":{"algo":"rsa","size":2048}}' \
   | cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=req.json -hostname "*.service.$STACK,*.service.$DATACENTER.$STACK" - \
   | cfssljson -bare "wildcard-user-$DATACENTER-services"
else
   echo "wildcard-user-services.pem exists. keeping previous cert. delete first if you want to regenerate it" >&2
fi
popd >/dev/null 2>&1
EOF

chmod +x "$CERTDIR/run.sh"

which cfssl > /dev/null 2>&1
if [ $? -eq 0 ]; then
    exec "$CERTDIR"/run.sh
else
    which docker > /dev/null 2>&1
    if [ $? -eq 0 ] && [ -S /var/run/docker.sock ]; then
        docker run --rm -v "$CERTDIR":"$CERTDIR" -u $(id -u):$(id -g) --entrypoint /bin/bash cfssl/cfssl -c "$CERTDIR/run.sh"
    else
        echo "couldn't find nor cfssl nor docker binary." >&2
        exit 1
    fi
fi

rm -f "$CERTDIR/run.sh"
find "$CERTDIR" -type f -exec chmod 0600 {} \;

if [ ! -f "$CERTDIR/client.pfx" ]; then
  if [ -f "$CERTDIR/cert.pem" ] && [ -f "$CERTDIR/key.pem" ]; then
    echo "Generating PKCS12 cert to use in browser"
    openssl pkcs12 -inkey "$CERTDIR/key.pem" -in "$CERTDIR/cert.pem" -export -out "$CERTDIR/client.pfx"
  fi
fi
echo "certs are in $(readlink "$CERTDIR")"
