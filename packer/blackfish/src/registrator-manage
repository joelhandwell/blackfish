#!/bin/bash
source "$(dirname "$0")/functions.sh"

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-hv] [-n CONTAINER_NAME] start|stop|join|rm
Handles the lifecycle of a registrator service docker container

COMMANDS:
    start              Starts a registrator
    stop               Stops a registrator
    rm                 rm rkt container

OPTIONS:
    -h                 display this help and exit
    -n CONTAINER_NAME  sets the name of the docker container.
                       Defaults to $REGISTRATOR_CONTAINER_NAME
    -v                 verbose mode. Can be used multiple
                       times for increased verbosity.
EOF
}

start(){
    log user.info "start"
    rkt run \
        --net=host \
        --hostname="$(hostname)" \
        --volume resolv,kind=host,source=/etc/resolv.conf,readOnly=false \
        --mount volume=resolv,target=/etc/resolv.conf \
        --volume dockersocket,kind=host,source=/var/run/docker.sock,readOnly=true \
        --volume cacert,kind=host,source=/etc/blackfish/certs/ca.pem,readOnly=true \
        --volume cert,kind=host,source=/etc/blackfish/certs/node.pem,readOnly=true \
        --volume key,kind=host,source=/etc/blackfish/certs/node-key.pem,readOnly=true \
        --mount volume=dockersocket,target=/tmp/docker.sock \
        --mount volume=cacert,target=/tmp/cacert \
        --mount volume=cert,target=/tmp/cert \
        --mount volume=key,target=/tmp/key \
        --uuid-file-save "$UUID_FILE" \
        --set-env=CONSUL_HTTP_SSL=true \
        --set-env=CONSUL_HTTP_SSL_VERIFY=true \
        --set-env CONSUL_CACERT=/tmp/cacert \
        --set-env CONSUL_TLSCERT=/tmp/cert \
        --set-env CONSUL_TLSKEY=/tmp/key \
        $(aci_id "gliderlabs/registrator") --exec /bin/registrator -- \
        -ip="$(getpubipaddr)" -cleanup -resync 300 -ttl 60 -ttl-refresh 15 --deregister always "consul-tls://consul:8500"
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts ":hv:" opt; do
    case "$opt" in
        h)
            show_help
            exit 0
            ;;
        v)  verbose=$((verbose+1))
            ;;
        '?')
            show_help >&2
            exit 2
            ;;
    esac
done
shift "$((OPTIND-1))" # Shift off the options and optional --.

case $1 in
    start)
        start
        ;;
    stop)
        stop_rkt
        ;;
    rm)
        rm_rkt
        ;;
    *)
        echo "unhandled command: $1" >&2
        exit 3
        ;;
esac
