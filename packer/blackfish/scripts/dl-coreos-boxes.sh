#!/bin/bash

BUILDDIR=./builds/
OPENSTACK_IMG_MD5_CHECKSUM=4a63e2055a0c3e16e6d26f427f3ec7e9
OPENSTACK_IMG=coreos_production_openstack_image.img
VBOX_STABLE_IMG_MD5_CHECKSUM=972fd22b5d3855c7628c6354b8a6b66f
VBOX_STABLE_IMG=coreos-stable.box
VBOX_ALPHA_IMG_MD5_CHECKSUM=49d426b18cae78778c8999b4ee52f874
VBOX_ALPHA_IMG=coreos-alpha.box

mkdir -p $BUILDDIR
pushd $BUILDDIR

check_img() {
    [[ "$(md5sum $1 | awk '{print $1}')" == "$2" ]]
}

# dl openstack
if [ ! -f ./$OPENSTACK_IMG ] || ! check_img $OPENSTACK_IMG $OPENSTACK_IMG_MD5_CHECKSUM; then
    wget https://beta.release.core-os.net/amd64-usr/current/coreos_production_openstack_image.img.bz2 -O - | bzcat > $OPENSTACK_IMG

    if ! check_img $OPENSTACK_IMG $OPENSTACK_IMG_MD5_CHECKSUM; then
        echo "image is corrupted" >&2
        exit 1
    fi
fi

mkdir -p openstack/latest
cat > openstack/latest/user_data <<EOF
#cloud-config
ssh_authorized_keys:
 - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key
EOF


# dl vagrant alpha
if [ ! -f ./$VBOX_STABLE_IMG ] || ! check_img $VBOX_STABLE_IMG $VBOX_STABLE_IMG_MD5_CHECKSUM; then
	  wget -O ./coreos-stable.box http://stable.release.core-os.net/amd64-usr/current/coreos_production_vagrant.box
    if ! check_img $VBOX_STABLE_IMG $VBOX_STABLE_IMG_MD5_CHECKSUM; then
        echo "image is corrupted" >&2
        exit 1
    fi
fi
tar -C ./coreos-stable -xzf ./coreos-stable.box

if [ ! -f ./$VBOX_ALPHA_IMG ] || ! check_img $VBOX_ALPHA_IMG $VBOX_ALPHA_IMG_MD5_CHECKSUM; then
	  wget -O ./coreos-stable.box http://alpha.release.core-os.net/amd64-usr/current/coreos_production_vagrant.box
    if ! check_img $VBOX_ALPHA_IMG $VBOX_ALPHA_IMG_MD5_CHECKSUM; then
        echo "image is corrupted" >&2
        exit 1
    fi
fi
tar -C ./coreos-alpha -xzf ./coreos-alpha.box




popd
