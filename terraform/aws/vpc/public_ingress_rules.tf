resource "aws_security_group_rule" "public_from_internet_http" {
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "public_from_internet_https" {
  type = "ingress"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks = ["0.0.0.0/0"]
}
