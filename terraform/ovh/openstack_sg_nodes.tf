resource "openstack_networking_secgroup_v2" "nodes_sg" {
  name = "${var.stack_name}_nodes_sg"
  description = "blackfish nodes sg"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_to_all_tcp" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 1
  port_range_max = 65535
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_to_all_udp" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 1
  port_range_max = 65535
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_ssh" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 22
    port_range_max = 22
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_control" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 4443
  port_range_max = 4443
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_swarm" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 4000
    port_range_max = 4000
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_docker_registry" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 5000
    port_range_max = 5000
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_consul_agent" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8500
    port_range_max = 8501
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_consul_dns_tcp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8600
    port_range_max = 8600
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_consul_dns_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8600
    port_range_max = 8600
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_dns_tcp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 53
    port_range_max = 53
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_dns_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 53
    port_range_max = 53
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_consul_cluster_tcp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8300
    port_range_max = 8302
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_bastion_consul_cluster_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8300
    port_range_max = 8302
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.bastion_sg.id}"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_public_tcp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 32768
    port_range_max = 60999
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_ip_prefix = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_public_web" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 80
  port_range_max = 80
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_public_ssl" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 443
  port_range_max = 443
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}
resource "openstack_networking_secgroup_rule_v2" "nodes_from_public_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 32768
    port_range_max = 60999
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_nodes_tcp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 32768
  port_range_max = 60999
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_nodes_udp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 32768
  port_range_max = 60999
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_consul_cluster_tcp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 8300
  port_range_max = 8302
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_consul_cluster_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8300
    port_range_max = 8302
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_consul_agent" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 8500
    port_range_max = 8501
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_consul_dns_tcp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 8600
  port_range_max = 8600
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_consul_dns_udp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 8600
  port_range_max = 8600
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_dns_tcp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 53
  port_range_max = 53
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_dns_udp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 53
  port_range_max = 53
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_docker" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 2375
  port_range_max = 2376
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_swarm" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 4000
  port_range_max = 4000
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_docker_network_tcp" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 7946
  port_range_max = 7946
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_docker_network_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 7946
    port_range_max = 7946
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_docker_network2_udp" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 4789
    port_range_max = 4789
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_docker_registry" {
    direction = "ingress"
    ethertype = "IPv4"
    port_range_min = 5000
    port_range_max = 5000
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
    remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_web" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 80
  port_range_max = 80
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_web_ssl" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 443
  port_range_max = 443
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "nodes_from_node_control_ssl" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 4443
  port_range_max = 4443
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
  remote_group_id   = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}
