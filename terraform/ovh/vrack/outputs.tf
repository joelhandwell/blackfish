output "os_username" {
  value = "${ovh_publiccloud_user.terraform.username}"
}

output "os_password" {
  value = "${ovh_publiccloud_user.terraform.password}"
}

output "os_tenant_name" {
  value = "${ovh_publiccloud_user.terraform.openstack_rc.OS_TENANT_NAME}"
}

output "os_tenant_id"   {
  value = "${ovh_publiccloud_user.terraform.openstack_rc.OS_TENANT_ID}"
}

output "os_auth_url"    {
  value = "${ovh_publiccloud_user.terraform.openstack_rc.OS_AUTH_URL}"
}

output "os_net_ids.GRA1" {
  value = "${ovh_publiccloud_private_network.bf_net.os_net_ids.GRA1}"
}
output "os_net_ids.BHS1" {
  value = "${ovh_publiccloud_private_network.bf_net.os_net_ids.BHS1}"
}
output "os_net_ids.SBG1" {
  value = "${ovh_publiccloud_private_network.bf_net.os_net_ids.SBG1}"
}
