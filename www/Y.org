#+TITLE: Docker in production
#+DATE: 2016/06/09 _ Ippon Technologies 
#+AUTHOR: @yanndegat
#+COMPANY: Ippon Technologies
#+EMAIL: ydegat@ippon.fr
#+LANGUAGE: en
#+WWW: http://blog.ippon.fr/
#+GITLAB: http://gitlab.com/blackfish
#+TWITTER: @yanndegat
#+REVEAL_ROOT: .
#+REVEAL_THEME: black
#+REVEAL_TRANS: slide
#+OPTIONS: toc:1 num:nil

* A little prelude about Devops
#+BEGIN_CENTER
#+ATTR_HTML: :width 600px
[[./images/devops.png]]
#+END_CENTER

#+BEGIN_NOTES
Dev    : the css master, the angular dev, the java dev, nodejs dev, the dba, scala dev
Devops : chef master, anisble fanboy, the dev that wanted to be an op, the one who runs linux@home, the op who unzipped the War, a pythonist 
Sysop  : the network engineer, the security engineer, the system engineer, the storage engineer, the  * engineer

Noops == I don't talk to no op, because they do their job very well and expose all the "infrastructure as a service"
#+END_NOTES

* Docker in one sentence
#+ATTR_HTML: :width 400px
#+BEGIN_CENTER
[[file:images/docker.jpg]]
#+END_CENTER

** Some say : "a lightweight VM" >>> NAh!
#+BEGIN_NOTES
some compare containers to "lightweight Virtual Machines"
other to "chroot on steroids", solaris zones etc. 
#+END_NOTES

Definitely not a VM.

** much more something like 
#+BEGIN_NOTES
It's mostly a new way to package "portable" apps
Remember why Java was so popular in the 00's? 
Because it was the first time people could deploy and run a software build on a platform on any other one.
So that developers could dev on Windows and Ops deploy on AIX, and all the tech industry was happy.
Docker now allows you to deploy any kind of software ( C/C++/Java/C#/JS/Scheme/Go/Python/.... ) on every platform.
#+END_NOTES

#+ATTR_REVEAL: :frag (appear)
- a pkg manager with a run cmd ...
  #+BEGIN_SRC bash
  $ yum run nginx
  #+END_SRC
- ... and a bit of ssh
  #+BEGIN_SRC bash
  $ ssh user@mydevhost yum run nginx
  #+END_SRC
- available on every platform


** In one sentece 

Docker (and alikes) is the JVM of the 20's

** BTW, have you had a look at nix/nixos ? 
#+BEGIN_CENTER
- https://www.mpscholten.de/docker/2016/01/27/you-are-most-likely-misusing-docker.html
- http://lethalman.blogspot.com/2016/04/cheap-docker-images-with-nix_15.html
#+END_CENTER

#+BEGIN_NOTES
Nix is an "functional" package manager
Allowing you to install apps on a box with no side effects
Some tried to demonstrate how to achieve some of docker's goals with nix.
#+END_NOTES
** and bocker ? 
#+BEGIN_CENTER
Docker implemented with 100 lines of bash:

https://github.com/p8952/bocker 
#+END_CENTER

#+BEGIN_NOTES
a very good way to understand & demystify docker's mechanics
#+END_NOTES
** bocker run 
#+BEGIN_SRC bash
function bocker_run() { 
  [...]
	ip link add devveth0_"$uuid" type veth peer name veth1_"$uuid"
  [...]
	ip netns exec netns_"$uuid" ip addr add 10.0.0."$ip"/24 dev veth1_"$uuid"
	[...]
	btrfs subvolume snapshot "$btrfs_path/$1" "$btrfs_path/$uuid" > /dev/null
	echo 'nameserver 8.8.8.8' > "$btrfs_path/$uuid"/etc/resolv.conf
	echo "$cmd" > "$btrfs_path/$uuid/$uuid.cmd"
	cgcreate -g "$cgroups:/$uuid"
	: "${BOCKER_CPU_SHARE:=512}" && cgset -r cpu.shares="$BOCKER_CPU_SHARE" "$uuid"
	: "${BOCKER_MEM_LIMIT:=512}" && cgset -r memory.limit_in_bytes="$((BOCKER_MEM_LIMIT * 1000000))" "$uuid"
	cgexec -g "$cgroups:$uuid" \
		ip netns exec netns_"$uuid" \
		unshare -fmuip --mount-proc \
		chroot "$btrfs_path/$uuid" \
		/bin/sh -c "/bin/mount -t proc proc /proc && $cmd" \
		2>&1 | tee "$btrfs_path/$uuid/$uuid.log" || true
	ip link del dev veth0_"$uuid"
	ip netns del netns_"$uuid"
}
#+END_SRC

** Where is the magic ?
#+BEGIN_CENTER
#+ATTR_HTML: :width 400px
[[file:images/magic.jpg]]
#+END_CENTER

#+BEGIN_NOTES
So what's new with docker ? If you could do the quite same before with solaris zones, 100 lines of bash or a package manager ...
#+END_NOTES

** The tools Docker leverages

Don't focus on the docker engine ant its flaws

By defining a defacto standard of how to build, package & run an app

#+ATTR_REVEAL: :frag (appear)
- simple packaging : dockerfile
- simple deployment configuration: docker-compose, fleet, pods
- great schedulers: swarm, mesos, kubernetes, rancher ...
- storage manangement : flocker, convoy
- new Cloud OSes : coreos, rancheros
- ...
- unikernels !!

** As we say today : "as simple as a Dockerfile"

"If only we could build VMs with a conf file as simple as a Dockerfile"

#+BEGIN_SRC bash
FROM iojs

ADD ./app.js /

CMD [ "iojs", "app.js" ]
#+END_SRC

** Have you said microservices ?

12 Factors apps + docker == microservices

#+BEGIN_SRC yaml
version: "2"
services:
  elasticsearch:
    image: elasticsearch

  logstash:
    image: logstash
    command: |
      -e 'input {[...]output { elasticsearch { hosts => ["elasticsearch:9200"] [...]}}'
    ports:
      - "12201:12201/udp"

  kibana:
    image: kibana
    ports:
      - "5601:5601"
    environment:
      ELASTICSEARCH_URL: http://elasticsearch:9200
#+END_SRC

** DEMO

A little example : 
- docker run alpine
- docker run nodejs example
- docker-compose up ELK

* From Dev to Prod
#+BEGIN_CENTER
#+ATTR_HTML: :width 500px
[[file:images/production.png]]
#+END_CENTER
** Things you better know about
#+BEGIN_CENTER
#+ATTR_HTML: :height 400px
[[file:images/docker-prod.jpg]]
#+END_CENTER
#+BEGIN_NOTES
There's a lot !
#+END_NOTES
** What do you need to go to prod ?

Back to basics

#+ATTR_REVEAL: :frag (appear)
- Package your app
- Describe its dependencies
- Ship it to your target environment
- Run / Scale / Upgrade it continuously
- Make it secure and accessible
- Backup / Restore it
- Monitor it

** The Twevles 
#+BEGIN_NOTES
If you want to containerize an app, you have to know that you better respect some principles
And that's how docker will force your developpers to release "cloud ready" apps.
#+END_NOTES

Wait, is my app "docker ready" ?

12 factors, or at least:
#+ATTR_REVEAL: :frag (appear)
- configuration by env vars
- stateless
- port bindings
- backing services
- fast startups
- dev/prod parity
- logs

** Compose not so compose-"able"

#+BEGIN_NOTES
You can easily describe an app dependencies ( such as sidekicks containers, DBs, LBs ) etc, in a docker-compose file
but for production you end up with very complex files. ( see graylog2 example ).
Some use the compose file as a spec, and embed it for production either manually or with python scripts.

A compose file can be considered as a minimal spec written by developpers for operators

there are other tools such as fleet, rancher compose, kubernetes pods that will be better suited
#+END_NOTES

How can i deploy my app, which is a microservices architeture with:
#+ATTR_REVEAL: :frag (appear)
- a react frontend
- a nodejs api
- a reactive java app based on akka
- and i need elasticsearch and redis and mongo

** Shipping

Where do i "upload" my brand new docker container? 

The docker registry is your new maven/pip/npm/gem/deb/rpm/ repo. 

#+BEGIN_SRC bash
$ docker tag myapp:latest prod-registry/myapp:1.1
$ docker push  prod-registry/myapp:1.1
#+END_SRC

#+BEGIN_NOTES
Use several registries in your continuous delivery workflow

gitlab registry, plus one registry per environment
#+END_NOTES

** Blue/Green Deployments
#+BEGIN_NOTES
To run, scale or upgrade your apps, you have to rely on service discovery tools
combined with load balancers auto reload.

Define your Blue green deployment workflow to upgrade your app and its dependencies
#+END_NOTES

How can i push my new app with zero downtime ? 

#+ATTR_REVEAL: :frag (appear)
- Rely on your load balancers
- don't forget to implement healthchecks everywhere
- no magic: you still have to define deployment workflows according to your usecase.

** Knock knock?

#+BEGIN_NOTES
You cannot publih your app on a constantly moving random IP:PORT
you need names and make your apps available through standard ports ( 80, 443 )

service discovery tools are the keystone of containerized microservices environments.

consul etcd zookeeper eureka

consul is a major player with a lot of great features ( dns, healthcheck kv/store, master election, locks )
and very simple to bootstrap in a secure manner

#+END_NOTES

Hey! where's my app? and who's behind:

http://172.17.0.4:32771 ?
http://172.17.10.10:32772 ?
http://172.18.3.7:32768 ?

** Security

docker run == telnet 

#+BEGIN_SRC bash
$ docker run -v /:/host alpine rm -Rf /host....
#+END_SRC


#+ATTR_REVEAL: :frag (appear)
- Public/Admin networks
- Encrypt with TLS everywhere
- VM isolation
- Remote Access Granted (secure access through TLS certs)
- Manage TLS certs as you would with SSH keys
** High availability
#+BEGIN_NOTES
if you can scale, but you cannot control the scheduling of your workloads, you'll get into trouble

Plus : you want to be able to reschedule a container from one node to another
If it's a node with persistent data... you'll need a volume plugin driver
#+END_NOTES

Tell me...

#+ATTR_REVEAL: :frag (appear)
- Are my 3 replicas on the same machine ?
- What happens to my containers if one node dies ?
- How can i backup my filesystems ?

** Supervision
#+BEGIN_NOTES
There's a lot of ways to monitor your system
cadvisor, collectd, telegraf, 
elk graylog 

- docker ps/stats
- cadvisor
- docker ucp
- weave scope
- collectd, telegraf, riemann
- influxdb, grafana, chronograf
- prometheus

you'll have to scripts and cron cleaning jobs
#+END_NOTES

how will i

#+ATTR_REVEAL: :frag (appear)
- ship logs
- collect metrics
- avoid "no space left on device"

** About performances
*** All about IO
Containerizing an app basically means : 
#+ATTR_REVEAL: :frag (appear)
- run it behind a linux bridge
- on a chrooted "Union Mount" filesystem (unionfs,aufs,overlayfs,...)

*** Networking
#+BEGIN_NOTES
a lot of initiative, you have to make a choice
#+END_NOTES

bypass the linux bridge

#+ATTR_REVEAL: :frag (appear)
- net host
- use a network plugin coreos flannel, weave net (+++), calico, docker networking

*** Volumes
#+BEGIN_NOTES
Buggy before 1.11 engine. Massive file descriptor leak.
Personnaly didn't try with engine > 10.3, waiting for coreos...
#+END_NOTES

bypass unionfs

#+ATTR_REVEAL: :frag (appear)
- mount a volume from host
- use a convoy, rexray, flocker

* Platforms : Docker Swarm & consort

** Make your choice!

A lot of platforms tries to help try to build a coherent system with their own answers and choices.

Choose one and try to stick (recommended) to it or build it yourself.

Most of the market is moving to kubernetes

You still have to know that each "community" platform is a puzzle with missing pieces.

** Swarm ( + swarmkit )

#+ATTR_REVEAL: :frag (appear)
- The Docker inc Way
- Presents cluster of nodes as a single one
- Tries to keep things as simple as a "docker run"
- recently introduced swarmkit

** Mesos
#+ATTR_REVEAL: :frag (appear)
- The Elder
- Container or Not
- You have to code "mesos" apps
- Hard to upgrade
- Manager nodes
- Marathon
- Can deploy Kubernetes and Swarm

** Kubernetes
#+ATTR_REVEAL: :frag (appear)
- Years of google engineering behind
- Prod ready ?
- "Ops"
- breaks the dev/prod parity

** Deis

#+ATTR_REVEAL: :frag (appear)
- an heroku like paas
- deploy with git push
- built on top of coreos
- migrating to kubernetes

** Mantl
#+ATTR_REVEAL: :frag (appear)
- by cisco cloud
- terraform + kubernetes/mesos + monitoring + consul + marathon
- seems to be a great project

** Rancher / RancherOS
#+ATTR_REVEAL: :frag (appear)
- An OS AND a Platform
- cool kido
- sidekicks support
- Docker compose Way
- seems to be hard to automate deployment of nodes
- Cool features ( convoy, control center )
- supports multipe implementation for each layer

** CoreOS

#+ATTR_REVEAL: :frag (appear)
- An OS, a scheduler, a network driver, a service discovery, a disributed filesystem
- AND a container engine ( rocket )
- a great building block
- A Docker Inc "Enemy"
- Promotes kubernetes

** and?

* Introducing Blackfish
#+BEGIN_CENTER
#+ATTR_HTML: :width 400px
[[file:images/blackfish_logo.png]]
#+END_CENTER

** Y?
#+ATTR_REVEAL: :frag (appear)
- simple to reason with
- doesn't require control nodes
- a low overhead on your nodes (~5%)
- docker way

** Architecture 

*** Usage
#+BEGIN_CENTER
#+ATTR_HTML: :width 600px
[[file:images/global_archi.png]]
#+END_CENTER


*** Components
#+BEGIN_CENTER
#+ATTR_HTML: :height 600px
[[file:images/coreos_inside.png]]
#+END_CENTER


*** Network

Terraformed AWS VPC

#+BEGIN_CENTER
#+ATTR_HTML: :height 600px
[[file:images/aws_vpc.png]]
#+END_CENTER

*** Grow your cluster

The Nodes.yml

#+BEGIN_CENTER
#+ATTR_HTML: :width 700px
[[file:images/swarm_ha.jpg]]
#+END_CENTER

** Dev & Aws ready

A cluster up & ready in less than 5 minutes

#+ATTR_REVEAL: :frag (appear)
- vagrant up
- vagrant up --provider=aws ( soon terraform )
- a complete multi az vpc with bastion+openvpn server instance

** OS 
#+BEGIN_CENTER
#+ATTR_HTML: :width 600px
[[file:images/coreos.png]]
#+END_CENTER

Why CoreOS ?

#+ATTR_REVEAL: :frag (appear)
- auto update
- good building blocks ( systemd / rkt )
- cloudinit + ignition + coreos-baremetal

** System services

Why rkt ?

#+ATTR_REVEAL: :frag (appear)
- run system services with rkt
- prevents from remote docker kill/rm
- better integration with systemd

** Private Repository

Why a private registry ?

#+ATTR_REVEAL: :frag (appear)
- You need to push your private images to your environment
- You can also push public images so that your cluster won't download from internet

** Service Discovery

Why consul ?

#+ATTR_REVEAL: :frag (appear)
- consul vs etcd vs zookeeper
- dns
- dns SRV requests
- registrator
- health check
- consul-template

** Port management

Why Haproxy ?

#+ATTR_REVEAL: :frag (appear)
- http / https
- haproxy-consul
- tcp/udp ( DBs )
- Logical Ports Mapping

** Centralized logs

How does it work ?

#+ATTR_REVEAL: :frag (appear)
- journald
- graylog vs ELK
- GELF Format
- custom forwarder

** Monitoring

How does it work ?

#+ATTR_REVEAL: :frag (appear)
- telegraf versus collectd vs riemann
- influxdb vs prometheus
- grafana
- kapacitor

** Security

How does it work ?

#+ATTR_REVEAL: :frag (appear)
- TLS everywhere
- Certificate Management
- PKI

** DEMO
#+ATTR_REVEAL: :frag (appear)
- Vagrant up
- Show a pre configured AWS cluster
- Run a Graylog cluster
- Add nodes

* The Rise of Unikernels
#+BEGIN_CENTER
#+ATTR_HTML: :width 500px
[[file:images/unikernels.png]]
#+END_CENTER

#+ATTR_REVEAL: :frag (appear)
- Better Isolation
- Better performance
- MirageOS

* Takeways

#+BEGIN_NOTES
Docker defines the same kind of frontier IAAS defined between Infrastructure and the rest of the world.
It will help you build your "heroku" a help you enter continuous delivery
CAAS delivers the promise of PAAS 

Docker will enforce dev <> ops collaboration as it clearly defines the frontier and the responsabilities of each other

#+END_NOTES
#+ATTR_REVEAL: :frag (appear)
- From dev to prod, you'll need a lot of energy
- Dev + Ops + Docker == Love
- There's no magic, you have to be infrastructure aware (ssd, network, az)
- docker-compose is not relevant for production (and never claimed to be)
- Use official images and stick to the "docker way" as much as possible
- Constantly evolving ecosystem
- Every platform is a puzzle with missing pieces and hidden bugs
- Still a bleeding edge technology (don't have a look at bugfixes in recent release notes)
- But get on board, you won't regret it. Because it's


* Thank you

  
